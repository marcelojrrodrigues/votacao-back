--CREATE database votacao

CREATE TABLE system_user (
	id int8 NOT NULL,
	email varchar(255) NULL,
	full_name varchar(255) NULL,	
	password varchar(255) NULL,
	user_name varchar(255) NULL,
	ativo bool NULL,
	CONSTRAINT system_user_pkey PRIMARY KEY (id)
);

create table profiles (system_user_id int8 not null, profiles int4);
alter table profiles add constraint FK_system_user foreign key (system_user_id) references system_user;


CREATE TABLE recurso (
	id int8 NOT NULL,
	descricao varchar(255) NULL,
	CONSTRAINT recurso_pkey PRIMARY KEY (id)
);


CREATE TABLE voto (
	id int8 NOT NULL,
	comentario varchar(255) NULL,
	usuario_id int8 NULL,
	recurso_id int8 NULL,
	"data" timestamp NULL,
	CONSTRAINT voto_pkey PRIMARY KEY (id),
	CONSTRAINT FK_system_user_id FOREIGN KEY (usuario_id) REFERENCES system_user(id),
	CONSTRAINT FK_recurso_id FOREIGN KEY (recurso_id) REFERENCES recurso(id)
);

CREATE SEQUENCE public.hibernate_sequence
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	CACHE 1
	NO CYCLE;

INSERT INTO public."system_user"
(id, email, "password", ativo, full_name, user_name)
VALUES(nextval('hibernate_sequence'), 'admin@admin.com', '$2a$10$HWHZFmkOlT/h5NXxmmd/oemHIAABBdqQ82FI.ofZdWqa6dMZilsx2', true, 'Admin', 'admin@admin.com');

DO $$ 
DECLARE
   idUser integer := (select id from "system_user" where email like 'admin@admin.com');
BEGIN 
	INSERT INTO public.profiles
	(system_user_id, profiles)
	VALUES(idUser, 2);   
END $$;


