package com.votacao.serivice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import com.votacao.model.GenericEntity;

public class GenericService<Entity extends GenericEntity, Repository extends CrudRepository<Entity, Long>> implements IGenericService<Entity> {

	@Autowired
	protected Repository repository;
	
	@Override
	public List<Entity> findAll() {
		return (List<Entity>) repository.findAll();
	}

	@Override
	public Entity save(Entity entity) {
		return repository.save(entity);
	}

	@Override
	public Optional<Entity> findById(long id) {
		return repository.findById(id);
	}

	@Override
	public void delete(Entity entity) {
		repository.delete(entity);
	}

	@Override
	public void deleteById(long id) {
		repository.deleteById(id);
	}

	@Override
	public long count() {
		return repository.count();
	}

}
