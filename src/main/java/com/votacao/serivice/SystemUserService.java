package com.votacao.serivice;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.votacao.model.SystemUser;
import com.votacao.model.SystemUserResponse;
import com.votacao.repository.SystemUserRepository;
import com.votacao.security.UserSecurity;
import com.votacao.serivice.exceptions.EmailInvalidException;
import com.votacao.serivice.exceptions.UserNameExitsException;

@Service
public class SystemUserService extends GenericService<SystemUser, SystemUserRepository> implements UserDetailsService {
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<SystemUser> systemUser = repository.findByUserName(username);
		
		if (systemUser.isPresent()) {
			return new UserSecurity(systemUser.get().getId(),
					systemUser.get().getUserName(), systemUser.get().getPassword(), systemUser.get().getProfiles());
			
		} else {
			throw new UsernameNotFoundException(username);
			
		}
	}
	
	public List<SystemUserResponse> findAllNoPassword() {
		List<SystemUser> users = (List<SystemUser>) this.repository.findAll();
		List<SystemUserResponse> usersResponse = new ArrayList<SystemUserResponse>();
		for (SystemUser systemUser : users) {
			usersResponse.add(new SystemUserResponse(systemUser.getId(),
													 systemUser.getFullName(),
													 systemUser.getEmail(),
													 systemUser.getUserName(),
													 systemUser.isAtivo()));		
		}
		return usersResponse;
	}
	
	
	@Override
	public SystemUser save(SystemUser entity) {
		if (!isEmailValid(entity.getEmail()))
			throw new EmailInvalidException();
		
		Optional<SystemUser> usuarioCadastrado = repository.findByUserName(entity.getUserName());
		if (usuarioCadastrado.isPresent()) {
			throw new UserNameExitsException();
		}
		
		entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
		return super.save(entity);
	}
	
	static Boolean isEmailValid(String email) {
	      String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	      return email.matches(regex);
	   }

}
