package com.votacao.serivice.exceptions;

import org.springframework.security.core.AuthenticationException;

public class EmailInvalidException extends AuthenticationException {
	
	private static final long serialVersionUID = 1L;

	public EmailInvalidException() {
		super("Email invalido!");
	}

}
