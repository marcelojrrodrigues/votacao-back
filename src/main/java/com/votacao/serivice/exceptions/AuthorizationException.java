package com.votacao.serivice.exceptions;

public class AuthorizationException extends RuntimeException {

	private static final long serialVersionUID = 263325744571629797L;

	public AuthorizationException(String msg) {
		super(msg);
	}
	
	public AuthorizationException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
