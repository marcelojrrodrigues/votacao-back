package com.votacao.serivice.exceptions;

public class VotoException extends RuntimeException {
	
	
	private static final long serialVersionUID = 1L;

	public VotoException(String msg) {
		super(msg);
	}

}
