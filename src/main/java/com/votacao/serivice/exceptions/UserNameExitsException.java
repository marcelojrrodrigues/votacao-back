package com.votacao.serivice.exceptions;

public class UserNameExitsException extends RuntimeException {
	
	
	private static final long serialVersionUID = 1L;

	public UserNameExitsException() {
		super("Usuario com este email já cadastrado no sistema!");
	}

}
