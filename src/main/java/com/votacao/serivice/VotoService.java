package com.votacao.serivice;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import com.votacao.model.Voto;
import com.votacao.repository.VotoRepository;
import com.votacao.serivice.exceptions.VotoException;

@Service
public class VotoService extends GenericService<Voto, VotoRepository> {

	@Override
	public Voto save(Voto entity) {
		Boolean votou = this.repository.existeVotoUsuario(entity.getUsuario().getId()) > 0;
		if (votou)
			throw new VotoException("Seu voto já foi computado para um recurso!");
		
		entity.setData(LocalDateTime.now());
		return super.save(entity);
	}
	
	public Long quantidadesVotosRecurso(Long idRecurso) {
		return this.repository.quantidadesVotosRecurso(idRecurso);
	}
	
}
