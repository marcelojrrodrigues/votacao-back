package com.votacao.serivice;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.votacao.model.Recurso;
import com.votacao.repository.RecursoRepository;

@Service
public class RecursoService extends GenericService<Recurso, RecursoRepository> {

	@Autowired
	VotoService votoService;
	
	@Override
	public List<Recurso> findAll() {
		List<Recurso> recursos = super.findAll();
		for (Recurso recurso : recursos) {
			recurso.setQuantidadeVotos(votoService.quantidadesVotosRecurso(recurso.getId()));
		}
		Collections.sort(recursos, new Comparator<Recurso>() {
			@Override
			public int compare(Recurso o1, Recurso o2) {				
				return o2.getQuantidadeVotos().compareTo(o1.getQuantidadeVotos());
			}		
		});
		return recursos;
	}
	
}
