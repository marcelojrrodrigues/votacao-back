package com.votacao.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.votacao.model.Recurso;
import com.votacao.serivice.RecursoService;

@RestController
@RequestMapping("/recurso")
public class RecursoController {

	@Autowired
	RecursoService recursoService;	
	
	@PostMapping
	@PreAuthorize("hasAnyRole('ROLE_ADM')")
	public Recurso salvar(@RequestBody Recurso recurso) throws Exception {		
		return recursoService.save(recurso);
	}
	
	@GetMapping("/findAll")
	public List<Recurso> findAll() throws Exception {		
		return recursoService.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Recurso> find(@PathVariable("id") Long id) throws Exception {		
		return recursoService.findById(id);
	}
	
	@PutMapping
	@PreAuthorize("hasAnyRole('ROLE_ADM')")
	public void update(@RequestBody Recurso recurso) throws Exception {		
		recursoService.save(recurso);
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyRole('ROLE_ADM')")
	public void delete(@PathVariable("id") Long id) throws Exception {		
		recursoService.deleteById(id);
	}
	
}
