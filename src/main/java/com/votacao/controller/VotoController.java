package com.votacao.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.votacao.entityRequest.VotoRequest;
import com.votacao.model.Recurso;
import com.votacao.model.SystemUser;
import com.votacao.model.Voto;
import com.votacao.serivice.VotoService;

@RestController
@RequestMapping("/voto")
public class VotoController {

	@Autowired
	private VotoService votoService;
	
	@PostMapping
	public Voto salvar(@RequestBody VotoRequest votoRequest) throws Exception {
		Voto voto = new Voto();
		voto.setRecurso(new Recurso(votoRequest.getIdRecurso()));
		voto.setUsuario(new SystemUser(votoRequest.getIdUsuario()));
		return votoService.save(voto);
	}
	
	@GetMapping("/{id}")
	public Optional<Voto> find(@PathVariable("id") Long id) throws Exception {		
		return votoService.findById(id);
	}
	
	@PutMapping
	public Voto update(@RequestBody Voto voto) throws Exception {
		return votoService.save(voto);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Long id) throws Exception {		
		votoService.deleteById(id);
	}
	
}
