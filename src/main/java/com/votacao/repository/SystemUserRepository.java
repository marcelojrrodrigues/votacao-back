package com.votacao.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.votacao.model.SystemUser;

@Repository
public interface SystemUserRepository extends CrudRepository<SystemUser, Long> {

	Optional<SystemUser> findByUserName(String userName);
	
}
