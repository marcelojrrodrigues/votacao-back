package com.votacao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.votacao.model.Recurso;

@Repository
public interface RecursoRepository extends CrudRepository<Recurso, Long>{
	
}
