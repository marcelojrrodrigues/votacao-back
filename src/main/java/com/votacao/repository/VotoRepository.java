package com.votacao.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.votacao.model.Voto;

@Repository
public interface VotoRepository extends CrudRepository<Voto, Long>{

	@Query("SELECT count(*) FROM Voto v WHERE v.usuario.id = ?1")
	Long existeVotoUsuario(Long idUsuario);
	
	@Query("SELECT COUNT(*) FROM Voto v WHERE v.recurso.id = ?1 ")
	Long quantidadesVotosRecurso(Long idRecurso);
	
}
