package com.votacao.sistemEnum;

public enum Profile {

	USER(1, "ROLE_USER", "USER"),
	ADM(2, "ROLE_ADM", "USER");
	
	private Integer id;
	private String type;
	private String nome;
	
	private Profile(Integer id, String type, String nome) {
		this.id = id;
		this.type = type;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getType() {
		return type;
	}
	
	public String getNome() {
		return nome;
	}
	
	public static Profile toEnum(Integer id) {
		for (Profile profile : Profile.values()) {
			if (profile.getId().compareTo(id) == 0) {
				return profile;
			}
		}
		
		return null;
	}
}
