package com.votacao.security;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.votacao.sistemEnum.Profile;

public class UserSecurity implements UserDetails{
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String userName;
	private String password;
	private Collection<? extends GrantedAuthority> authorities;
	
	public UserSecurity() {
		
	}
	
	public UserSecurity(Long id, String userName, String password, Set<Profile> profiles) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.authorities = profiles.stream().map(profile -> new SimpleGrantedAuthority(profile.getType())).collect(Collectors.toList());
	}

	public Long getId() {
		return id;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public boolean hasRole(Profile profile) {
		return this.getAuthorities().contains(new SimpleGrantedAuthority(profile.getType()));
	}

}
