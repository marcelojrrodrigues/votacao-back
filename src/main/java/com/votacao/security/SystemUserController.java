package com.votacao.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.votacao.model.Reponse;
import com.votacao.model.SystemUser;
import com.votacao.model.SystemUserResponse;
import com.votacao.repository.SystemUserRepository;
import com.votacao.serivice.SystemUserService;
import com.votacao.serivice.UserService;

@RestController
@RequestMapping("/users")
public class SystemUserController {

	@Autowired
	private SystemUserRepository userRepository;
	
	@Autowired
	private SystemUserService userService;
	
	@PostMapping("/sign-up")
	public ResponseEntity<Reponse> signUp(@RequestBody SystemUser user) throws Exception {

		try {
			userService.save(user);
			return new ResponseEntity<Reponse>(new Reponse("Usuário Cadastrado"), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(new Reponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
			
	}
	
	@GetMapping("/findAll")
	public List<SystemUserResponse> findAll(){
		return userService.findAllNoPassword();
		
	}
	
}
