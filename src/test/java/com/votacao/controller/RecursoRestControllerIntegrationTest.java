//package com.votacao.controller;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.Arrays;
//import java.util.List;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import com.votacao.model.Recurso;
//import com.votacao.serivice.RecursoService;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(RecursoController.class)
//public class RecursoRestControllerIntegrationTest {
//
//	@Autowired
//    private MockMvc mvc;
// 
//    @MockBean
//    private RecursoService service;
//    
//    @Test
//    public void findAllRecurso() throws Exception {
//         
//        Recurso recurso = new Recurso();
//        recurso.setDescricao("teste");        
//     
//        List<Recurso> allRecursos = Arrays.asList(recurso);
//     
//        given(service.findAll()).willReturn(allRecursos);
//     
//        mvc.perform(get("/recurso")
//          .contentType(MediaType.APPLICATION_JSON))
//          .andExpect(status().isOk())
//          .andExpect(jsonPath("$", hasSize(1)))
//          .andExpect(jsonPath("$[0].name", is(alex.getName())));
//    }
//
//}
