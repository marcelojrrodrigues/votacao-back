# Titulo

Projeto Votação de Recursos

## Introdução
Utiliza-se neste projeto tecnologia Java no backend com o framework spring boot, o frontend foi desenvolvido com VUE js.


### Pré-requisitos
[BACKEND]
É necessário ter instalado o Java 8 JDK. https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html
É recomendado utilizar como IDE o Eclipse IDE https://www.eclipse.org/downloads/

[FRONTEND]
É indispensável ter o Node instalado (Recomendo a última versão estável). https://nodejs.org/en/


### Instalando

Ao obter o projeto BACKEND no seu repositório do git é essencial atualizar as dependências do Maven.
Posteriormente, é fundamental a criação do banco de dados "votacao". Abra seu gerenciador de banco de dados e crie uma conexão e o database.
database - votacao
usuario - postgres
senha - postgres
porta - 5432

Logo após a configuração do banco de dados rode o projeto backend como "spring boot app", os scripts da estrutura do sistema rodarão automaticamente pelo flyway.
Verifique se a estrutura foi criada corretamente no banco de dados.
 
Depois da configuração do backend, seguimos com a instalação do projeto FRONTEND. Os projetos foram construídos separadamente, siga com o download do repository do FRONTEND https://gitlab.com/marcelojrrodrigues/votacao-front
Após o download do repository é recomendado a utilização do editor de texto visual studo code https://code.visualstudio.com/Download
Com o node instalado caminhe até a pasta raiz do projeto FRONTEND e instale suas dependências com o comando "npm install".
Em seguida, podemos servir o projeto FRONTEND com o comando "npm run serve".

### Endpoints

Rota de usuarios
/users
[POST]/sign-up - Rota liberada para cadastro de usuários.
[POST]/login - Retorna o token para acesso aos serviços
[GET]/findAll - rota liberada, utilizada por sistemas externos.
[GET]/id
[PUT]/
[DELETE] 

Rota de recursos
/recurso
[POST]/ - Metodo salvar.
[GET]/findAll - Lista todos os recursos.
[GET]/id - busca um recurso pelo id.
[PUT]/
[DELETE]

os recursos poderão ser consumidos por uma api externa bastanto obter um token de autenticação disparando um [POST] para a roda de login com o seguinte JSON:
EX:
{
	"userName": "admin@admin.com",
	"password": "123"
}
após esse processo apenas adicione o token obtido nas requisições para a api.

### Usuario admin
Foi inserido um usuário administrtador padrão no sistema
login: admin@admin.com
password: 123

### Contato
email: marcelojrrodrigues@hotmail.com